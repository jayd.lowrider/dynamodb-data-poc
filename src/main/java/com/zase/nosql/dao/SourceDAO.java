package com.zase.nosql.dao;

import com.zase.nosql.pojo.Source;

public interface SourceDAO {

	public abstract Source create(Source object);

	public abstract Source update(Source object);

	public abstract void delete(Source object);

	public abstract Source findById(String id);
	
	public abstract Source updateWithConditionals(Source obj);
}