package com.zase.nosql.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.zase.nosql.pojo.index.SourceUUIDIndex;

@Repository
public class SourceUUIDIndexImplDAO extends AWSAbstractDAO<SourceUUIDIndex> implements
		SourceUUIDIndexDAO {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(SourceUUIDIndexImplDAO.class);
	
	
	public String findSourceID(String uuid) {
		// TODO Auto-generated method stub
		SourceUUIDIndex object = mapper.load(SourceUUIDIndex.class, uuid);
		return object.getSourceId();
	}

	@Override
	public SourceUUIDIndex create(SourceUUIDIndex object) {
		// TODO Auto-generated method stub
		 mapper.save(object, new DynamoDBMapperConfig(SaveBehavior.CLOBBER));
		 return object;
	}

	@Override
	public SourceUUIDIndex update(SourceUUIDIndex object) {
		// TODO Auto-generated method stub
		mapper.save(object, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		return object;
	}
	
	public void delete(String uuid){
	 	SourceUUIDIndex index = mapper.load(SourceUUIDIndex.class, uuid, new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.EVENTUAL));
	 	delete(index);
	}

	@Override
	public void delete(SourceUUIDIndex object) {
		// TODO Auto-generated method stub
		mapper.delete(object);
	}

	
	@Override
	public SourceUUIDIndex findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}


}
