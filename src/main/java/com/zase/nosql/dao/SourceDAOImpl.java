package com.zase.nosql.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.zase.nosql.pojo.Source;
import com.zase.nosql.pojo.exceptions.DynamoDBConcurrencyException;
import com.zase.nosql.pojo.utils.DynamoDBReflectionUtils;

@Repository
public class SourceDAOImpl extends AWSAbstractDAO<Source> implements SourceDAO {
	
	private static final Logger LOG = LoggerFactory.getLogger(SourceDAOImpl.class);
	
	/* (non-Javadoc)
	 * @see com.zase.nosql.dao.SourceDAO#create(com.zase.nosql.pojo.Source)
	 */
	public Source create(Source object){
		mapper.save(object, new DynamoDBMapperConfig(SaveBehavior.CLOBBER));
		return object;
	}

	/* (non-Javadoc)
	 * @see com.zase.nosql.dao.SourceDAO#update(com.zase.nosql.pojo.Source)
	 */
	@Override
	public Source update(Source object) {
		mapper.save(object, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		return object;
	}

	/* (non-Javadoc)
	 * @see com.zase.nosql.dao.SourceDAO#delete(com.zase.nosql.pojo.Source)
	 */
	@Override
	public void delete(Source object) {
		mapper.delete(object); // new DynamoDBDeleteExpression()
	}

	
	/* (non-Javadoc)
	 * @see com.zase.nosql.dao.SourceDAO#findById(java.lang.String)
	 */
	@Override
	public Source findById(String id) {
		return mapper.load(Source.class, id,
				new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.EVENTUAL));
	}
	
	/**
	 *	Update with Conditionals specifically check the values for consistency,
	 * that is to say that your attributes are not stale vs. the copy of dynamoDB. 
	 */
	public Source updateWithConditionals(Source obj){
		
		assert obj != null;
		assert obj.getId() != null;
		assert obj.getId().isEmpty();
		
		Source persistentObject =  findById(obj.getId());
		if (persistentObject !=null &&
				  persistentObject.getVersion().equals(obj.getVersion())){	
				
			try{
				Map<String, ExpectedAttributeValue> expectedValues = new HashMap<String, ExpectedAttributeValue>();
				Map<String, Object> ol = findUpdatedAttribute(persistentObject, obj);
				
				for (java.util.Map.Entry<String, Object> e : ol.entrySet()){
					ExpectedAttributeValue v = new ExpectedAttributeValue();
					v.setValue( new AttributeValue(String.valueOf(e.getValue())));
					
					String name = e.getKey().replace(GET_PREFIX, "").toLowerCase();
					name = name.replace(IS_PREFIX, "").toLowerCase();
					expectedValues.put(name, v);
				}
				
				DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
				saveExpression.setExpected(expectedValues);
				
				mapper.save(obj, saveExpression, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
				return obj;
			}
			catch(Exception e){
				LOG.error("Conditionals thrown error.", e);
				return null;
			}
		}
		else if (persistentObject != null) {
			throw new DynamoDBConcurrencyException("Our database version for this object=[" + 
					obj.getClass().getName() + "] with id="+ obj.getId() + " has current version=" + obj.getVersion() + 
					" dynamoDB version=" + persistentObject.getVersion());
		}
		else {
			throw new DynamoDBConcurrencyException("This object may have been deleted. PersistentObject=" + obj.getClass().getName() + " id=" + obj.getId() + " version=" + obj.getVersion());
		}
	}
	
	
	private Map<String, Object> findUpdatedAttribute(Source persistentObject, Source obj ) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		
		java.util.Collection<Method> relevantGetters =  DynamoDBReflectionUtils.getRelevantGetters(persistentObject.getClass());
		Map<String, Object> data = new HashMap<String, Object>();
		
		//now look if there are difference.
		for (Method m : relevantGetters){
			m.setAccessible(true);
			Object orig = m.invoke(persistentObject, null);
			Object update = m.invoke(obj, null);
			
			//Can we use equals() or computed hashCode()
			if (!orig.equals(update)){
				data.put(m.getName(),orig);
			}
		}
		
		return data;
	}
	
}
