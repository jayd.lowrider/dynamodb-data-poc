package com.zase.nosql.dao;

import javax.inject.Inject;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;


public abstract class AWSAbstractDAO<T> {
	
	protected static final String GET_PREFIX = "get";
	protected static final String IS_PREFIX ="is";
	
	@Inject
	protected AmazonDynamoDBClient client;

	@Inject
	protected DynamoDBMapper mapper;

	
	public abstract T create(T object);
	public abstract T update(T object);
	public abstract void delete(T object);
	public abstract T findById(String id);
	
}
