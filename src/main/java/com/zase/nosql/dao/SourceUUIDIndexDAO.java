package com.zase.nosql.dao;

import com.zase.nosql.pojo.index.SourceUUIDIndex;

public interface SourceUUIDIndexDAO  {
	
	String findSourceID(String uuid);
	
	public abstract SourceUUIDIndex create(SourceUUIDIndex object);
	public abstract SourceUUIDIndex update(SourceUUIDIndex object);
	public abstract void delete(SourceUUIDIndex object);
	public abstract void delete(String uuid);
	
}
