package com.zase.nosql.pojo.exceptions;

public class DynamoDBConcurrencyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1734441200763998561L;
	
	public DynamoDBConcurrencyException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public DynamoDBConcurrencyException(String s){
		super(s);
	}
}
