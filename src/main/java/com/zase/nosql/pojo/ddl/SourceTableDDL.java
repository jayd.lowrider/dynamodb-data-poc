package com.zase.nosql.pojo.ddl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.util.Tables;
import com.zase.nosql.pojo.Source;


/**
 * 
 * Read and Write throughput was computed by the following:
 * 
 * 1) We know the size of Source is 64bytes, so the object is well under the 4K or 1K size limit.
 * 2) Request of data from gateway is:
 *           a) 6 Heartbeat/minute
 *           b) 2 DeviceData/minute
 *           c) 1 DiscoveryData/minute
 * 3) DynamoDB reads is # reads per second * 4KB (on consistent * 2 on eventual)
 * 4) DynamoDB writes is # writes per second * 1KB.
 * 5) Estimate is 1 Gateway on 254 devices.
 *           a) 6/60 * 254 = 25 read per second.
 *           b) 2/60 * 254 = 8 reads per second.
 *           c) 1/60 * 254 = 4 reads per second.
 * 6) Total 25 + 8 + 4 = 37.          
 * 
 * @author mramos
 *
 */

public class SourceTableDDL {

	public static void createTable(AmazonDynamoDBClient client) {

		try {
			
			CreateTableRequest createTableRequest = new CreateTableRequest()
					.withTableName(Source.TABLE_NAME);

			//key.
			createTableRequest.withKeySchema(
					new KeySchemaElement()
						.withAttributeName(Source.KEY_PK.getColumnName()).withKeyType(KeyType.HASH));
	
			createTableRequest
					.withAttributeDefinitions(new AttributeDefinition()
							.withAttributeName(Source.KEY_PK.getColumnName()).withAttributeType(
									Source.KEY_PK.getScalarType()));
			
			//TODO: If you don't need ranges then don't add this because you will need to provide it when 
			// dynamo is doing a lookup (load())
			
//			createTableRequest.withKeySchema(
//					new KeySchemaElement()
//						.withAttributeName(Source.RANGE_PK.getColumnName()).withKeyType(KeyType.RANGE));
//	
//			createTableRequest
//					.withAttributeDefinitions(new AttributeDefinition()
//							.withAttributeName(Source.RANGE_PK.getColumnName()).withAttributeType(
//									Source.RANGE_PK.getScalarType()));
//	
			
			createTableRequest
					.withProvisionedThroughput(new ProvisionedThroughput()
							.withReadCapacityUnits(37L).withWriteCapacityUnits(
									37L));
			TableDescription createdTableDescription = client.createTable(
					createTableRequest).getTableDescription();
			System.out.println("Created Table: " + createdTableDescription);
			Tables.waitForTableToBecomeActive(client, Source.TABLE_NAME);
//			// Wait for it to become active
//			waitForTableToBecomeAvailable(Source.TABLE_NAME);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (AmazonClientException e) {
			e.printStackTrace();
		}

	}
}
