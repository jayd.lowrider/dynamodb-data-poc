package com.zase.nosql.pojo.ddl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.util.Tables;
import com.zase.nosql.pojo.index.SourceUUIDIndex;

public class SourceUUIDIndexTableDDL {
	public static void createTable(AmazonDynamoDBClient client) {

		try {
			CreateTableRequest createTableRequest = new CreateTableRequest()
					.withTableName(SourceUUIDIndex.TABLE_NAME);
			
			//key.
			createTableRequest.withKeySchema(new KeySchemaElement()
					.withAttributeName(SourceUUIDIndex.KEY_PK.getColumnName()).withKeyType(KeyType.HASH));
			createTableRequest
					.withAttributeDefinitions(new AttributeDefinition()
							.withAttributeName(SourceUUIDIndex.KEY_PK.getColumnName()).withAttributeType(
									SourceUUIDIndex.KEY_PK.getScalarType()));
			
			createTableRequest
					.withProvisionedThroughput(new ProvisionedThroughput()
							.withReadCapacityUnits(37L).withWriteCapacityUnits(
									37L));
			TableDescription createdTableDescription = client.createTable(
					createTableRequest).getTableDescription();
			System.out.println("Created Table: " + createdTableDescription);
//			// Wait for it to become active
			Tables.waitForTableToBecomeActive(client, SourceUUIDIndex.TABLE_NAME);
//			waitForTableToBecomeAvailable(Source.TABLE_NAME);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (AmazonClientException e) {
			e.printStackTrace();
		}

	}
}
