package com.zase.nosql.pojo.index;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.zase.nosql.pojo.Source.KeyDataTypePair;

@DynamoDBTable(tableName=SourceUUIDIndex.TABLE_NAME)
public class SourceUUIDIndex {
	
	public static final String TABLE_NAME="SourceUUIDIndex";
	private static final Map<String, ScalarAttributeType> TABLE_ATTRS = new HashMap<String, ScalarAttributeType>();
	public static String[] COLUMN_FAMILY;
	
	public static final KeyDataTypePair KEY_PK = new KeyDataTypePair( "uuid", ScalarAttributeType.S );
	
	static {
		TABLE_ATTRS.put("sourceId", ScalarAttributeType.S); 
		COLUMN_FAMILY = TABLE_ATTRS.keySet().toArray(new String[ TABLE_ATTRS.size() ]);
	}
	
	public static final ScalarAttributeType getTableAttributeType(String data){
		return TABLE_ATTRS.get(data);
	}
	
	private String sourceId;
	private String uuid;
	
	
	@DynamoDBHashKey
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@DynamoDBAttribute
	public String getSourceId() {
		return sourceId;
	}
	
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
}
