package com.zase.nosql.service;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.zase.nosql.dao.SourceDAO;
import com.zase.nosql.dao.SourceUUIDIndexDAO;
import com.zase.nosql.pojo.Source;
import com.zase.nosql.pojo.index.SourceUUIDIndex;

@Component(value="SourceService")
public class SourceService {
	
	@Inject
	SourceDAO sourceDAO;
	
	@Inject
	SourceUUIDIndexDAO sourceUUIDDao;
	
	public void create( Source source ){
		
		Source r = sourceDAO.create(source);
		
		//create index
		SourceUUIDIndex index = new SourceUUIDIndex();
		index.setSourceId(r.getId());
		index.setUuid(r.getUuid());
		
		sourceUUIDDao.create(index);
	}
	
	public void delete(String id){
		Source r = findById(id);
		delete(r);
	}
	
	public void delete(Source s){
		
		sourceUUIDDao.delete(s.getUuid());
		sourceDAO.delete(s);
	}
	
	public void update(Source source){
		Source r = sourceDAO.update(source);
	}
	
	public void updateWithConditionals(Source s){
		sourceDAO.updateWithConditionals(s);
	}
	
	public Source findById(String sourceId){
		return sourceDAO.findById(sourceId);
	}	
	
	public Source findSourceByUUID(String uuid){
		String sourceId = sourceUUIDDao.findSourceID(uuid);
		return sourceDAO.findById(sourceId);
	}
	
}	
