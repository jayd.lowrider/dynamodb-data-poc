package com.zase.nosql.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;
import com.zase.nosql.pojo.Source;
import com.zase.nosql.pojo.ddl.SourceTableDDL;
import com.zase.nosql.pojo.ddl.SourceUUIDIndexTableDDL;
import com.zase.nosql.pojo.index.SourceUUIDIndex;
import com.zase.nosql.service.SourceService;

public class SourceTest {
	
	private static final Logger LOG = LoggerFactory.getLogger(SourceTest.class);

	private ApplicationContext beanFactory=null;
	private String UUID=java.util.UUID.randomUUID().toString();
	java.util.Random rnd = new Random();
	private AmazonDynamoDBClient dynamoDB;
	
	private SourceService sourceService;
	
	public static final void main(String[] args) throws InterruptedException {
		SourceTest test = new SourceTest();
		
		test.runDB();
		
		System.exit(0);
	}
	
	public SourceTest(){
		beanFactory = new ClassPathXmlApplicationContext("classpath*:root/test-resource.xml");
		dynamoDB = (AmazonDynamoDBClient) beanFactory.getBean("amazonDynamoDB");
		sourceService = (SourceService) beanFactory.getBean("SourceService");
	}
	
	public void runDB() throws InterruptedException{
		
		if ( !areTableActive(Source.TABLE_NAME, dynamoDB) ){
			SourceTableDDL.createTable(dynamoDB);
		}
		
		if ( !areTableActive(SourceUUIDIndex.TABLE_NAME, dynamoDB) ){
			SourceUUIDIndexTableDDL.createTable(dynamoDB);
		}
		
		sanityTest();
		//testConcurrentAccess();
//		
		testNReadsAndWrites(100);
		//testNReads(2000);
		//testNWrites(1000);
		//testUpdateWithConditionals();
		
		Thread.currentThread().join();
	}
	
	private void sanityTest(){
		Source s = new Source();
		s.setIp_address("192.168.23.34");
		s.setLocation("Chicago");
		s.setName("QLN320");
		s.setUuid(UUID);
		s.setSerialNumber("234-1123-2234");
		testCRUD(s);
	}
	
	
	public void testUpdateWithConditionals(){
		
		final Source s = createSource();
		sourceService.create(s);
		final Source t1=sourceService.findById(s.getId());
		assert t1.getId() == s.getId();
		
		final Source t2=sourceService.findById(s.getId());
		assert t2.getId() == s.getId();
		
		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		
		//copied 2X so I can debug (put breakpoints.)
		service.execute(new Runnable() {
			
			public void run() {
				t1.setIp_address("192.168.23.33");
				LOG.info("testUpdateWithConditionals()[ " + Thread.currentThread().getName() +   "] Start processing writes with conditionals ... ");
				long start = System.currentTimeMillis();
				sourceService.updateWithConditionals(t1);
				long end = System.currentTimeMillis() - start;
				LOG.info("testUpdateWithConditionals()[ " + Thread.currentThread().getName() +   "] End total time is " + TimeUnit.MILLISECONDS.toSeconds(end) + " in seconds.");
			}
		});
		
		service.execute(new Runnable() {
			
			public void run() {
				t2.setIp_address("192.168.23.35");
				LOG.info("testUpdateWithConditionals()[ " + Thread.currentThread().getName() +   "] Start processing writes with conditionals ... ");
				long start = System.currentTimeMillis();
				sourceService.updateWithConditionals(t2);
				long end = System.currentTimeMillis() - start;
				LOG.info("testUpdateWithConditionals()[ " + Thread.currentThread().getName() +   "] End total time is " + TimeUnit.MILLISECONDS.toSeconds(end) + " in seconds.");
			}
		});
	}
	
	public void testCRUD(Source s){
		
		sourceService.create(s);
		
		Source t=sourceService.findById(s.getId());
		
		assert t.getId() == s.getId();
		
		Source c=sourceService.findSourceByUUID(s.getUuid());
		assert c.getId() == s.getId();
		
		String oldLocation = s.getLocation();
		String newLocation = "Chicago, IL";
		s.setLocation(newLocation);
		
		sourceService.update(s);
		Source t1=sourceService.findById(s.getId());
		
		assert t1.getLocation().equalsIgnoreCase(newLocation);
		assert !t1.getLocation().equalsIgnoreCase(oldLocation);

		sourceService.delete(s);
		Source t2=sourceService.findById(s.getId());
		
		assert t2 == null;
	}
	
	private Source createSource(){
		Source s = new Source();
		s.setIp_address("192.168.23.34");
		s.setLocation("New York");
		s.setName("QLN" + rnd.nextInt() + "n" );
		s.setUuid(java.util.UUID.randomUUID().toString());
		s.setSerialNumber("234-" + rnd.nextInt());
		return s;
	}
	
	public void testNWrites(final int n) throws InterruptedException{
		
		ScheduledExecutorService service = Executors.newScheduledThreadPool(n/2);
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch waitForFinished = new CountDownLatch(n);
		final List<Long> timeSeries=new ArrayList<Long>(n);
		
		
		for (int i=0;i<=n-1; i++){
			service.execute(new Runnable() {
				
				public void run() {
					try {
						start.await();
						LOG.info("testNWrites("  + n + ")[ " + Thread.currentThread().getName() +   "] Start processing writes ... ");
						long start = System.currentTimeMillis();
						final Source s = createSource();
						sourceService.create(s);
						sourceService.delete(s);
						long end = System.currentTimeMillis() - start;
						LOG.info("testNWrites("  + n + ")[ " + Thread.currentThread().getName() +   "] End total time is " + TimeUnit.MILLISECONDS.toSeconds(end) + " in seconds.");
						timeSeries.add(end/1000L);
						waitForFinished.countDown();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		
		start.countDown();
		waitForFinished.await();
		printRelevantLogs(n, timeSeries);
	}
	
	
	public void testNReads(final int n) throws InterruptedException{
		
		ScheduledExecutorService service = Executors.newScheduledThreadPool(n/2);
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch waitForFinished = new CountDownLatch(n);
		final List<Long> timeSeries=new ArrayList<Long>(n);
		
		final Source s = createSource();
		sourceService.create(s);
		Source c=sourceService.findSourceByUUID(s.getUuid());
		assert c.getId() == s.getId();
		
		for (int i=0;i<=n-1; i++){
			service.execute(new Runnable() {
				
				public void run() {
					try {
						start.await();
						LOG.info("testNReads("  + n + ")[ " + Thread.currentThread().getName() +   "] Start processing read ... ");
						long start = System.currentTimeMillis();
						Source c=sourceService.findSourceByUUID(s.getUuid());
						assert c !=null;
						long end = System.currentTimeMillis() - start;
						LOG.info("testNRead("  + n + ")[ " + Thread.currentThread().getName() +   "] End total time is " + TimeUnit.MILLISECONDS.toSeconds(end) + " in seconds.");
						timeSeries.add(end/1000L);
						waitForFinished.countDown();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		
		start.countDown();
		waitForFinished.await();
		
		printRelevantLogs(n, timeSeries);
		
	}
	
	private void printRelevantLogs(int n, List<Long> timeSeries) throws InterruptedException{
		while (timeSeries.size() != n){
			TimeUnit.SECONDS.sleep(3L);
		}
		LOG.info("TimeSeries count=" + timeSeries.size());
		
		StringBuilder sb = new StringBuilder();
		for (Long l : timeSeries){
			sb.append(l).append(" ");
		}
		LOG.info(sb.toString());
	}
	
	public void testNReadsAndWrites(final int n) throws InterruptedException{
		
		ScheduledExecutorService service = Executors.newScheduledThreadPool(n/2);
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch waitForFinished = new CountDownLatch(n);
		final List<Long> timeSeries=new ArrayList<Long>(n);
		
		for (int i=0;i<=n-1; i++){
			
			service.execute(new Runnable() {
				
				public void run() {
					try {
						start.await();
						LOG.info("testNReadsAndWrites("  + n + ")[ " + Thread.currentThread().getName() +   "] Start processing read and writes ... ");
						long start = System.currentTimeMillis();
						Source s = createSource();
						testCRUD(s);
						long end = System.currentTimeMillis() - start;
						LOG.info("testNReadsAndWrites("  + n + ")[ " + Thread.currentThread().getName() +   "] End total time is " + TimeUnit.MILLISECONDS.toSeconds(end) + " in seconds.");
						timeSeries.add(end/1000L);
						waitForFinished.countDown();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		
		start.countDown();
		waitForFinished.await();
		
		printRelevantLogs(n, timeSeries);
	}
	
	
	public void testConcurrentAccess() throws InterruptedException{
		
		ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latchWait = new CountDownLatch(2);
		
		final Source s = new Source();
		s.setIp_address("192.168.23.34");
		s.setLocation("Chicago");
		s.setName("QLN320");
		s.setUuid(UUID);
		s.setSerialNumber("234-1123-2234");
		sourceService.create(s);
		
		final List<String> location = new ArrayList<String>(2);
		location.add("Arlington Heights, IL " + System.currentTimeMillis());
		location.add("Huntley, IL "+ System.currentTimeMillis());
		
		
		service.execute( new Runnable() {
			
			public void run() {
				try {
					LOG.info( "Thread 1 Location=" + location.get(0)  );
					latch.await();
					s.setLocation(location.get(0));
					sourceService.update(s);
					LOG.info( "Thread 1 Update call=" + System.currentTimeMillis()  );
					latchWait.countDown();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		service.execute( new Runnable() {
			
			public void run() {
				try {
					LOG.info( "Thread 2 Location=" + location.get(1)  );
					latch.await();
					s.setLocation(location.get(1));
					sourceService.update(s);
					LOG.info( "Thread 2 Update call=" + System.currentTimeMillis()  );
					latchWait.countDown();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Thread.sleep(2000);
		latch.countDown();
		latchWait.await();
		Source ss = sourceService.findById(s.getId());
		int t =  location.indexOf(ss.getLocation());
		LOG.info( " testConcurrentAccess() Source.getLocation() " + ss.getLocation() + " and the winner is Thread=" + (t+1));
		sourceService.delete(ss);
	}
	
	public boolean areTableActive(String tableName, AmazonDynamoDBClient client){
		try {
			DescribeTableRequest request = new DescribeTableRequest()
					.withTableName(tableName);
			TableDescription tableDescription = client.describeTable(request)
					.getTable();
			String tableStatus = tableDescription.getTableStatus();
			System.out.println("  - current state: " + tableStatus);
			if (tableStatus.equals(TableStatus.ACTIVE.toString()))
				return true;
		} catch (AmazonServiceException ase) {
			if (ase.getErrorCode()
					.equalsIgnoreCase("ResourceNotFoundException") == false)
				throw ase;
		}
		return false;
	}
	
}
